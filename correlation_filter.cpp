#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    int *copy = new int[height * width];
    int h,w;

    //filter 1
    //topmost row
    for(h=0,w=0; w<width; w++){
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[(1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }
    }
    
    //rest
    for(h = 1; h < height-1; ++h)
    {
        w=0;
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[(1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }

        for(w = 1; w < width-1; ++w)
        {
            copy[h * width + w] = 
              image[(-1 + h) * width -1 + w] * filter[0]
            + image[(-1 + h) * width + w] * filter[1]
            + image[(-1 + h) * width + 1 + w] * filter[2]
            + image[h * width -1 + w] * filter[ FILTER_SIZE]
            + image[h * width + w] * filter[FILTER_SIZE + 1]
            + image[h * width + 1 + w] * filter[FILTER_SIZE + 2]
            + image[(1 + h) * width -1 + w] * filter[2 * FILTER_SIZE]
            + image[(1 + h) * width + w] * filter[2 * FILTER_SIZE + 1]
            + image[(1 + h) * width + 1 + w] * filter[2 * FILTER_SIZE + 2];
        }

        w=width-1;
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[(1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }
    }

     //bottommost row
    for(h=height-1,w=0; w<width; w++){
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[(1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }
    }

    for(h = 0; h < height; ++h)
        {
            for(w = 0; w < width; ++w)
                image[h * width + w] = copy[h * width + w];
        }




    //filter 2
    //topmost row
    for(h=0,w=0; w<width; w++){
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }
    }
    
    //rest
    for(h = 1; h < height-1; ++h)
    {
        w=0;
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }

        for(w = 1; w < width-1; ++w)
        {
            copy[h * width + w] = 
              image[(-1 + h) * width -1 + w] * filter[FILTER_SIZE * FILTER_SIZE]
            + image[(-1 + h) * width + w] * filter[FILTER_SIZE * FILTER_SIZE + 1]
            + image[(-1 + h) * width + 1 + w] * filter[FILTER_SIZE * FILTER_SIZE + 2]
            + image[h * width -1 + w] * filter[FILTER_SIZE * FILTER_SIZE + FILTER_SIZE]
            + image[h * width + w] * filter[FILTER_SIZE * FILTER_SIZE + FILTER_SIZE + 1]
            + image[h * width + 1 + w] * filter[FILTER_SIZE * FILTER_SIZE + FILTER_SIZE + 2]
            + image[(1 + h) * width -1 + w] * filter[FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE]
            + image[(1 + h) * width + w] * filter[FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1]
            + image[(1 + h) * width + 1 + w] * filter[FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 2];
        }

        w=width-1;
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }
    }

    //bottommost row
    for(h=height-1,w=0; w<width; w++){
        copy[h * width + w] = 0;
        for(int offx = -1; offx <= 1; ++offx)
            {
                for(int offy = -1; offy <= 1; ++offy)
                {
                    if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
                        continue;
                    copy[h * width + w] += image[(offx + h) * width + offy + w] * 
                        filter[FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
                }
            }
    }

    for(h = 0; h < height; ++h)
        {
            for(int w = 0; w < width; ++w)
                image[h * width + w] = copy[h * width + w];
        }

    delete copy;
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];

                
    int filters = FILTERS;
    //bug fixed
    cin >> filters;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
