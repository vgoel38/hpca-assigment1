# HPCA Assignment
Optimize correlation filters using hardware counters.
Contained are two files:
* correlation_filter.cpp: File to be optimized
* create_image.cpp: Used to generate the input
### Compiling
```
g++ create_img.cpp -o generate
g++ correlation_filter.cpp -o filter
```
### Generating Input
```
./generate > input
```
### Running correlation filter
```
./filter < input
```
